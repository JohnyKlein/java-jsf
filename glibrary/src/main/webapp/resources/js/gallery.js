let firstTime = true;
$('#pagination-demo').twbsPagination({
  totalPages: 6,
  visiblePages: 6,
  next: 'Próximo',
  prev: 'Anterior',
  first: 'Primeiro',
  last: 'Último',
  onPageClick: function (event, page) {
	  if (!firstTime) {
		  loadBooksSearched(page);  
	  }
	  firstTime = false;
  }
});

function loadBooksSearched(page = null) {
  if (!page) page = $('.active .page-link').html();
  let pageIndex = getGoogleSearchIdxByPage(page);
  let name = $('#search-bar').val();
  let params = [{name:'pageIndex', value:pageIndex}, {name:'nameBook', value:name}];
  
  getGBooksByNameAndIndex(params);
}

function loadBooksFavorites(xhr, status, args) {
	if (xhr.readyState === 4) {
		setBooksHtml(args.search.books);
	}
}

function applyResponseOfRequestGbooks(xhr, status, args) {
	if (xhr.readyState === 4) {
		setBooksHtml(args.search.books);
	}
 }

function setBooksHtml(books) {
	getHtmlByBooksObjs(books).then(htmlContent => {
      $('#page-content').html(htmlContent);
    });
}

//async function getHtmlTotalBooks() {
//  let name = $('#search-bar').val();
//  let fullUrl = `http://${host}/books-total?name=${name}`;
//
//  return new Promise(resolve => {
//    if (!name) {
//      resolve(0);
//    } else {
//      $.get(fullUrl, function(valueTotal) {
//        resolve(valueTotal);
//      });
//    }
//  });
//}

async function getHtmlByBooksObjs(books) {
  return new Promise(resolve => {
    let html = '';
    let htmlItems = '';

    if (books.length === 0) {
      resolve("");
    } else {
      books.forEach(function(book, index) {
        getHtmlItemGallery(book).then(itemHtml => {
          htmlItems += itemHtml;
          if (index === books.length - 1) {
            html += '<div class="container">'
            html += '  <div class="row">';
            html +=     htmlItems;
            html += '  </div>'
            html += '</div>';
            resolve(html);
          }
        });
      });
    }
  });
}

async function getHtmlItemGallery(book) {
  return new Promise(resolve => {
      let html = '';
      let description = getHtmlItemsDescription(book.description);
      let htmlRemoveOrAdd = book.favorite ? getRemoveButtonHtml(book.id) : getAddButtonHtml(book.id);
       
      html += '<div class="col-4 item-gallery" style="margin-bottom: 35px;">';
      html += htmlRemoveOrAdd;
      html += ' <div class="thumbnail">';
      html += `   <img src="${book.thumbnail}">`;
      html += '   <div class="caption" style="margin: 5px;">';
      html += `     <h5>${book.title}</h5>`;
      html += `     ${description}`;
      html += '   </div>';
      html += ' </div>';
      html += '</div>';

      resolve(html);
    });
}

function getRemoveButtonHtml(bookId) {
	return `<button onclick="removeFavoriteBook('${bookId}')">(-) REMOVE FAVORITE</button>`;
}

function getAddButtonHtml(bookId) {
	return `<button onclick="addFavoriteBook('${bookId}')">(+) ADD FAVORITE</button>`;
}

function addFavoriteBook(bookId) {
	let params = [{name:'bookId', value:bookId}];
	addFirebaseBooks(params);
}

function removeFavoriteBook(bookId) {
	let params = [{name:'bookId', value:bookId}];
	removeFirebaseBooks(params);
}

function getHtmlItemsDescription(txtDescription) {
  txtDescription = capitalize(txtDescription);

  if (txtDescription) {
    return `<div class="col-12 text-left" style="padding:20px;">
      <h6>Descrição:</h6>
      <p>${txtDescription}</p>
    </div>`;
  }

  return '';
}

function capitalize(s) {
  if (typeof s !== 'string') return '';
  return s.charAt(0).toUpperCase() + s.slice(1);
}

function getGoogleSearchIdxByPage(page) {
  return (page-1) + "0";
}

$('#search-bar').keypress(function(event) {
  var keycode = (event.keyCode ? event.keyCode : event.which);
  
  //Verify enter event
  if (keycode == '13') {
    loadBooksSearched();
  }
});

$('#btn-search').click(function(item) {
  loadBooksSearched();
});