function generateFirebaseToken() {
	let email = document.getElementById('loginForm:userEmail').value;
	let password = document.getElementById('loginForm:userPassword').value;
	
    firebase.auth().signInWithEmailAndPassword(email, password).then(function(currentUser) {
    	currentUser.user.getIdToken(/* forceRefresh */ true).then(function(tokenValue) {
    		doLogin([{name:'token', value:tokenValue}]);
        }).catch(function(error) {
            alert(error);
        });
    }).catch(function(error) {
        alert(error.message);
    });
}