package com.glibrary.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;

import com.glibrary.bean.Book;
import com.glibrary.bean.Search;
import com.library.utils.GoogleBooksUtils;
import com.library.utils.SessionUtils;

public class BookGoogle {
	
    public static List<Book> getAllBooksByIndexName(Integer index, String nameBook) {
    	JSONObject response = GoogleBooksUtils.getBooksByRequestIndexName(index, nameBook);
    	return getBooksByResponse(response);
    }
    
    @SuppressWarnings("unchecked")
    private static List<Book> getBooksByResponse(JSONObject response) {
        List<Book> books = new ArrayList<>();
        List<HashMap<String, Object>> items = (List<HashMap<String, Object>>) response.toMap().get("items");
        List<Book> favoritesBooks = new Search().getFavoritesBooksSaved(SessionUtils.getUserId());

        if (items != null) {
            items.forEach(item -> {
                try {
                    String id = (String) item.get("id");
					HashMap<String, Object> volume = (HashMap<String, Object>) item.get("volumeInfo");
                    String title = (String) volume.get("title");
                    List<String> authors = (List<String>) volume.get("author");
                    String publisher = (String) volume.get("publisher");
                    String publishedDate = (String) volume.get("publishedDate");
                    String description = (String) volume.get("description");
                    String thumbnail = getThumbnail((HashMap<String, Object>) volume.get("imageLinks"));
                    boolean isFavorite = favoritesBooks.stream().map(Book::getId).collect(Collectors.toList()).contains(id);

                    books.add(new Book(id, title, authors, publisher, publishedDate, description, thumbnail, isFavorite));
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            });
        }

        return books;
    }
    
    private static String getThumbnail(HashMap<String, Object> volumeImage) {
        if (volumeImage != null) {
            if (volumeImage.get("thumbnail") != null) {
                return (String) volumeImage.get("thumbnail");
            }
        }
        return "";
    }

}
