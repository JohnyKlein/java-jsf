package com.glibrary.view;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;

import com.glibrary.bean.Book;
import com.glibrary.bean.Search;
import com.glibrary.bean.User;
import com.library.utils.ViewUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@SessionScoped
@ManagedBean(name="searchView")
public class SearchView {
	private Search search;
	
	public void getSearchFromJs(User user) {	
		try {			
		    Map<String, String> params = ViewUtils.getRemoteParameters();
		    Integer index = Integer.parseInt(Optional.ofNullable(params.get("pageIndex")).orElse("-1"));
		    String nameBook = params.get("nameBook");
		    List<Book> books = Book.getAllBooksByIndexName(index, nameBook);
		    search = new Search(books, index, nameBook, user);
		    
		    PrimeFaces.current().ajax().addCallbackParam("search", search);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void addFavoriteBook(Search search) {
		String bookId = StringUtils.defaultIfBlank(ViewUtils.getRemoteParameters().get("bookId"), "");
	    search.addFavoriteBook(bookId);
	}
	
	public void removeFavoriteBook(Search search) {
		String bookId = StringUtils.defaultIfBlank(ViewUtils.getRemoteParameters().get("bookId"), "");
	    search.removeFavoriteBook(bookId);
	}
}
