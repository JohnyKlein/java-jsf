package com.glibrary.view;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.commons.lang3.StringUtils;

import com.glibrary.bean.Authentication;
import com.glibrary.bean.User;
import com.library.utils.FirebaseUtils;
import com.library.utils.SessionUtils;
import com.library.utils.ViewUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SessionScoped
@ManagedBean(name="authenticationView")
public class AuthenticationView {
	
	private static final String AUTH_MESSAGE_ERROR = "Erro ao autenticar!";
	
	private Authentication authentication;
	
	@PostConstruct
	public void init() {
		authentication = new Authentication(new User());
	}
	
	public void doLogin() {
		String token = ViewUtils.getRemoteParameters().get("token");
		FirebaseUtils.setUserSessionByToken(token);
		
		if (authentication.isValidUser(token)) {
			authentication.getUser().setUserName(SessionUtils.getUserName());
			authentication.getUser().setPicture(SessionUtils.getUserPicture());
			authentication.getUser().setId(SessionUtils.getUserId());
			
		    ViewUtils.redirectTo("favorites.xhtml");
//		    ViewUtils.redirectTo("search.xhtml");
		} else {
			throw new RuntimeException(AUTH_MESSAGE_ERROR); //TODO
		}
	}
	
	public String getUserName() {
		return StringUtils.defaultIfBlank(getUser().getUserName(), getUser().getEmail());
	}
	
	public User getUser() {
		return authentication.getUser();
	}
	
	public String doLogout() {
		SessionUtils.doLogout();
		FirebaseUtils.shutdownFirebase();
		
		return "login";
	}
}