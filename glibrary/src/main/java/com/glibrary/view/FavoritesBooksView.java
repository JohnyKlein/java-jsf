package com.glibrary.view;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;

import com.glibrary.bean.Book;
import com.glibrary.bean.Search;
import com.glibrary.bean.User;
import com.library.utils.ViewUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@SessionScoped
@ManagedBean(name="favoritesBooksView")
public class FavoritesBooksView {
	
	public void getFavorites(User user) {
	    List<Book> books = new Search().getFavoritesBooksSaved(user);
	    PrimeFaces.current().ajax().addCallbackParam("search", new Search(books, null, null, user)); //TODO - Mudar obj
	}
		
	public void removeFavoriteBook(User user) {
		String bookId = StringUtils.defaultIfBlank(ViewUtils.getRemoteParameters().get("bookId"), "");
	    new Search(user).removeFavoriteBook(bookId);
	    
	    getFavorites(user);
	}
}