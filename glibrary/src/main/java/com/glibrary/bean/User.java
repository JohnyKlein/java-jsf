package com.glibrary.bean;

import org.apache.commons.lang3.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User {
	private String id = "";
	private String email = "";
	private String password = "";
	private String userName = "";
	private String picture = "";
	
	public String getUserName() {
		return StringUtils.defaultIfBlank(userName, email);
	}
}