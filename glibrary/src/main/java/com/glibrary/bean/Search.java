package com.glibrary.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Search { //TODO - Pode ser galeria
	private List<Book> books;
	private Integer pageIndex;
	private String wordSearched;
	private User user;
	
	public Search(User user) {
		this.user = user;
	}
	
	public List<Book> getFavoritesBooksSaved(User user) {
		return getFavoritesBooksSaved(user.getId());
	}
	
	public List<Book> getFavoritesBooksSaved(String userId) {
		List<Book> favoritesBooks = new ArrayList<>(); //TODO - Abstrair l�gica firebase
		Firestore db = FirestoreClient.getFirestore();
	    CollectionReference favorites = db.collection("favorites");
	    Query query = favorites.whereEqualTo("user.id", userId);
	    ApiFuture<QuerySnapshot> querySnapshot = query.get();

	    try {
			for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
			  Book book = new Book();
			  book.setId(document.getString("book.id"));
			  book.setThumbnail(document.getString("book.thumbnail"));
			  book.setDescription(document.getString("book.description"));
			  book.setTitle(document.getString("book.title"));
			  book.setFavorite(true);
			  
			  favoritesBooks.add(book);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		return favoritesBooks;
	}
	
	public void addFavoriteBook(String bookId) {
		Book book = books.stream()
				.filter(item->item.getId().equals(bookId))
				.findAny()
				.orElse(null);
		
		//TODO - Abstrair l�gica firebase
		HashMap<String, Object> userNode = new HashMap<String, Object>();
		userNode.put("id", user.getId());
		userNode.put("name", user.getUserName());
		
		HashMap<String, Object> bookNode = new HashMap<String, Object>();
		bookNode.put("id", book.getId());
		bookNode.put("title", book.getTitle());
		bookNode.put("authors", book.getAuthors());
		bookNode.put("publisher", book.getPublisher());
		bookNode.put("publishedDate", book.getPublishedDate());
		bookNode.put("description", book.getDescription());
		bookNode.put("thumbnail", book.getThumbnail());
		bookNode.put("isFavorite", book.isFavorite());
		
		HashMap<String, HashMap<String, Object>> docData = new HashMap<String, HashMap<String,Object>>();		
		docData.put("user", userNode);
		docData.put("book", bookNode);
		
		Firestore db = FirestoreClient.getFirestore();
		String documentId = book.getId() + user.getId();
		ApiFuture<WriteResult> future = db.collection("favorites").document(documentId).set(docData);
		
		try {
			System.out.println("Update time : " + future.get().getUpdateTime());
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	public void removeFavoriteBook(String bookId) {
		try {
			Firestore db = FirestoreClient.getFirestore();
			CollectionReference favorites = db.collection("favorites");
			User user = Optional.ofNullable(this.user).orElse(new User());
		    Query query = favorites.whereEqualTo("user.id", user.getId()).whereEqualTo("book.id", bookId);
		    ApiFuture<QuerySnapshot> querySnapshot = query.get();
		    QueryDocumentSnapshot documentSnapshot;
			documentSnapshot = querySnapshot.get().getDocuments().stream().findFirst().orElse(null);
			
	    	if (documentSnapshot != null) {
				documentSnapshot.getReference().delete();
				System.out.println("Deletado!");	
	    	} else {
	    		System.out.println("Documento n�o existe!");
	    	}
		} catch (InterruptedException | ExecutionException | NullPointerException e) {
			e.printStackTrace();
		}
	}
}
