package com.glibrary.bean;

import java.util.List;

import com.glibrary.dao.impl.BookGoogle;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Book extends BookGoogle {
	private String id;
    private String title;
    private List<String> authors;
    private String publisher;
    private String publishedDate;
    private String description;
    private String thumbnail;
    private boolean isFavorite;
    
}