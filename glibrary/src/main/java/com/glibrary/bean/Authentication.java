package com.glibrary.bean;


import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;

import com.library.utils.FirebaseUtils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@RequestScoped
public class Authentication {
	private User user;
	
    @PostConstruct
    public void init(){
    	user = new User();
    }
	
	public boolean isValidUser(String token) {		
		return user != null && FirebaseUtils.isValidToken(token);
	}
}