package com.library.utils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import org.json.JSONObject;

public class GoogleBooksUtils {
	
	private static final String URL_BASE = "https://www.googleapis.com/books/v1/volumes?maxResults=10&startIndex=";

    public static JSONObject getBooksByRequestIndexName(Integer index, String nameBook) {
    	String jsonResponse = null;
    	String path = URL_BASE + index + "&q=";
        
    	if (index != null && nameBook != null) {
	        try {
	            HttpResponse<String> httpResponse;
	            HttpClient client = HttpClient.newHttpClient();
	            String nameBookEncoded = URLEncoder.encode(nameBook, "UTF-8");
	            String fullPath = path + nameBookEncoded;
	            HttpRequest httpRequest = HttpRequest.newBuilder(new URI(fullPath))
	                    .header("Accept", "application/json")
	                    .header("Content-Type", "application/json")
	                    .timeout(Duration.ofMinutes(1))
	                    .GET()
	                    .build();
	
	            httpResponse = client.send(httpRequest, HttpResponse.BodyHandlers.ofString());
	            jsonResponse = httpResponse.body();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        } catch (URISyntaxException e) {
	            e.printStackTrace();
	        }
    	}

        return new JSONObject(jsonResponse);
    }

}
