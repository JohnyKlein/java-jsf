package com.library.utils;

import java.io.IOException;
import java.util.Map;

import javax.faces.context.FacesContext;

public class ViewUtils {
	
	public static Map<String, String> getRemoteParameters() {
	     return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
	}
	
	public static void redirectTo(String fileView) {
	    FacesContext context = FacesContext.getCurrentInstance();
	    try {
			context.getExternalContext().redirect(fileView);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
