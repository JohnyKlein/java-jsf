package com.library.utils;

import java.util.Optional;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtils {

	public static HttpSession getSession() {
		return (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(false);
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
	}
	
	private static String getSessionAttr(String attrName) {
		return (String) Optional.ofNullable(getSession().getAttribute(attrName)).orElse("");
	}

	public static String getUserName() {
		return getSessionAttr("userName");
	}

	public static String getUserId() {
		return getSessionAttr("userId");
	}
	
	public static String getUserPicture() {
		return getSessionAttr("userPicture");
	}
	
	public static void doLogout() {
		SessionUtils.getSession().invalidate();
	}
}
