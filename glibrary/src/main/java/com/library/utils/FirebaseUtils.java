package com.library.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpSession;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;

public class FirebaseUtils {
    private static final String FIREBASE_KEY_LOCATION = "C:\\library-gbooks-firebase-adminsdk.json"; //TODO - Chave em outro lugar
	private static final String FIREBASE_DATABASE_URL = "https://library-gbooks.firebaseio.com/";

	private static void initFirebase() throws FirebaseAuthException {		
        try {
            FileInputStream refreshToken = new FileInputStream(FIREBASE_KEY_LOCATION);
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(refreshToken))
                    .setDatabaseUrl(FIREBASE_DATABASE_URL)
                    .build();

            FirebaseApp.initializeApp(options);            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	public static void shutdownFirebase() {
		FirebaseApp.getInstance().delete();
	}

    public static void setUserSessionByToken(String idToken) {
        try {
        	if (idToken != null) {
        		if (FirebaseApp.getApps().isEmpty()) {
	        		initFirebase();
	                FirebaseToken verifyToken = FirebaseAuth.getInstance().verifyIdToken(idToken, true);

	                HttpSession session = SessionUtils.getSession();
	    			session.setAttribute("userId", verifyToken.getUid()); //TODO - Nomes como Enum
	    			session.setAttribute("userName", verifyToken.getName());
	    			session.setAttribute("userPicture", verifyToken.getPicture());
        		}
            }
        } catch (FirebaseAuthException e) {
            e.printStackTrace();
        } catch (Exception e) {
			e.printStackTrace();
		}
    }

	public static boolean isValidToken(String token) {
		return SessionUtils.getUserId() != null;
	}

}